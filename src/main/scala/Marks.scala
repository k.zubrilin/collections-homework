import scala.annotation.tailrec
import scala.io.Source

object Marks extends App {
  def readFromFile(path: String): List[String] = {
    val file = Source.fromFile(path)
    val strings = file.getLines.toList
    file.close
    strings
  }

  val fileText = readFromFile("marks.txt")

  @tailrec
  def formReport(text: List[String], report: Map[String, List[Int]]): Map[String, List[Int]] = {
    if (text.isEmpty) report
    else {
      val reg = "([a-zA-Z]+):([0-9]+)".r
      text.head match {
        case reg(name, mark) =>
          val marks = report.getOrElse(name, List[Int]()) :+ mark.toInt
          formReport(text.tail, report + (name -> marks))
      }
    }
  }

  val students = formReport(fileText, Map())

  println("Sorted list by sum of marks:")
  students.map(x => (x._1, x._2.sum)).toList.sortBy(-_._2).foreach(x => println(s"  ${x._1}: ${x._2}"))

  println("\nStudents' GPA:")
  students.map(x => (x._1, BigDecimal(x._2.sum.toDouble/x._2.length).setScale(2, BigDecimal.RoundingMode.HALF_UP)))
    .toList.sortBy(-_._2).foreach(x => println(s"  ${x._1}: ${x._2}"))

  val allMarks: List[Int] = students.values.flatten.toList.sorted
  println(s"""
             |Sum of marks: ${allMarks.sum}
             |Median of marks: ${allMarks(allMarks.length / 2)}
             |Mode of marks: ${allMarks.groupBy(identity).map(x => (x._1, x._2.size)).maxBy(_._2)._1}
             |""".stripMargin)
}